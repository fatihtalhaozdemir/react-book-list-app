import React, { Component } from "react";
import BookList from "./components/List";
import { Button } from "uikit-react";

import "./App.css";

class App extends Component {
	getBooks() {
		console.log("test");
	}

	render() {
		return (
			<div className="App uk-margin-large-top">
				<Button>Yenile</Button>

				<h3>Tüm Kitaplar</h3>

				<hr />

				<BookList />

				<hr />

				<div className="uk-container uk-container-small ">
					<h3>Kitap Ekle</h3>

					<form action="">
						<div className="uk-margin">
							<input
								className={"uk-input"}
								type="text"
								placeholder={"Kitap Adı"}
							/>
						</div>
						<div className="uk-margin">
							<textarea
								className={"uk-textarea"}
								name=""
								id=""
								cols="30"
								rows="10"
								placeholder={"Kitap Açıklaması"}
							></textarea>
						</div>
					</form>

					{/*<Button color="primary" onClick={ this.addBook() }>Ekle</Button>*/}
				</div>
				<hr />

				<div className="uk-container uk-container-small ">
					<h3>Kitap Değiştir</h3>

					<form action="">
						<div className="uk-margin">
							<input
								className={"uk-input"}
								type="text"
								placeholder={"Kitap Id"}
							/>
						</div>
						<div className="uk-margin">
							<input
								className={"uk-input"}
								type="text"
								placeholder={"Kitap Adı"}
							/>
						</div>
						<div className="uk-margin">
							<textarea
								className={"uk-textarea"}
								name=""
								id=""
								cols="30"
								rows="10"
								placeholder={"Kitap Açıklaması"}
							></textarea>
						</div>
					</form>

					<Button color="primary">Değiştir</Button>
				</div>
				<hr />

				<div className="uk-container uk-container-small uk-margin-large-bottom">
					<h3>Kitap Sil</h3>

					<form action="">
						<div className="uk-margin">
							<input
								className={"uk-input"}
								type="text"
								placeholder={"Kitap Id"}
							/>
						</div>
					</form>

					<Button color="primary">Sil</Button>
				</div>
			</div>
		);
	}
}

export default App;
