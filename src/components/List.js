import React, { Component } from "react";
import PropTypes from "prop-types";
import { List, ListItem } from "uikit-react";

class BookList extends Component {
	static propTypes = {
		list: PropTypes.array.isRequired,
	};
	refresh() {}
	addBook(something) {
		alert(something);
	}
	render() {
		return (
			<div className="App uk-margin-large-top">
				<List listItems={this.props.list}>
					<ListItem>
						<div className="uk-width-1-1 uk-margin-small-bottom">
							<b>ID:</b> 1
						</div>

						<div className="uk-width-1-1 uk-margin-small-bottom">
							<b>Title:</b> Title
						</div>

						<div className="uk-width-1-1 uk-margin-small-bottom">
							<b>Description:</b> Lorem ipsum dolor sit amet, consectetur
							adipisicing elit. Accusantium, aliquid atque dolor unde.
						</div>
					</ListItem>
				</List>
			</div>
		);
	}
}

export default BookList;
